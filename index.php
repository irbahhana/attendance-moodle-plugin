<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Activity attendance reports
 *
 * @package    report
 * @subpackage attendance
 * @copyright  2008 Sam Marshall
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once($CFG->libdir . '/completionlib.php');

define('COMPLETION_REPORT_PAGE', 500);

// Get course
$id = required_param('course', PARAM_INT);
$course = $DB->get_record('course', array('id' => $id));
if (!$course) {
    print_error('invalidcourseid');
}
$context = context_course::instance($course->id);

// Sort (default lastname, optionally firstname)
$sort = optional_param('sort', '', PARAM_ALPHA);
$firstnamesort = $sort == 'firstname';

// CSV format
$format = optional_param('format', '', PARAM_ALPHA);
$excel = $format == 'excelcsv';
$csv = $format == 'csv' || $excel;

// Paging
$start = optional_param('start', 0, PARAM_INT);
$sifirst = optional_param('sifirst', 'all', PARAM_NOTAGS);
$silast = optional_param('silast', 'all', PARAM_NOTAGS);
$start = optional_param('start', 0, PARAM_INT);

// Whether to show extra user identity information
$extrafields = get_extra_user_fields($context);
$leftcols = 1 + count($extrafields);

function csv_quote($value)
{
    global $excel;
    if ($excel) {
        return core_text::convert('"' . str_replace('"', "'", $value) . '"', 'UTF-8', 'UTF-16LE');
    } else {
        return '"' . str_replace('"', "'", $value) . '"';
    }
}

$url = new moodle_url('/report/attendance/index.php', array('course' => $id));
if ($sort !== '') {
    $url->param('sort', $sort);
}
if ($format !== '') {
    $url->param('format', $format);
}
if ($start !== 0) {
    $url->param('start', $start);
}
if ($sifirst !== 'all') {
    $url->param('sifirst', $sifirst);
}
if ($silast !== 'all') {
    $url->param('silast', $silast);
}
$PAGE->set_url($url);
$PAGE->set_pagelayout('report');

require_login($course);

// Check basic permission
require_capability('report/attendance:view', $context);

// Get group mode
$group = groups_get_course_group($course, true); // Supposed to verify group
if ($group === 0 && $course->groupmode == SEPARATEGROUPS) {
    require_capability('moodle/site:accessallgroups', $context);
}

// Get data on activities and progress of all users, and give error if we've
// nothing to display (no users or no activities)
$reportsurl = $CFG->wwwroot . '/course/report.php?id=' . $course->id;
$completion = new completion_info($course);
$activities = $completion->get_activities();

if ($sifirst !== 'all') {
    set_user_preference('ifirst', $sifirst);
}
if ($silast !== 'all') {
    set_user_preference('ilast', $silast);
}

if (!empty($USER->preference['ifirst'])) {
    $sifirst = $USER->preference['ifirst'];
} else {
    $sifirst = 'all';
}

if (!empty($USER->preference['ilast'])) {
    $silast = $USER->preference['ilast'];
} else {
    $silast = 'all';
}

// Generate where clause
$where = array();
$where_params = array();

if ($sifirst !== 'all') {
    $where[] = $DB->sql_like('u.firstname', ':sifirst', false);
    $where_params['sifirst'] = $sifirst . '%';
}

if ($silast !== 'all') {
    $where[] = $DB->sql_like('u.lastname', ':silast', false);
    $where_params['silast'] = $silast . '%';
}

// Get user match count
$total = $completion->get_num_tracked_users(implode(' AND ', $where), $where_params, $group);

// Total user count
$grandtotal = $completion->get_num_tracked_users('', array(), $group);

// Get user data
$progress = array();

if ($total) {
    $progress = $completion->get_progress_all(
        implode(' AND ', $where),
        $where_params,
        $group,
        $firstnamesort ? 'u.firstname ASC, u.lastname ASC' : 'u.lastname ASC, u.firstname ASC',
        $csv ? 0 : COMPLETION_REPORT_PAGE,
        $csv ? 0 : $start,
        $context
    );
}

if ($csv && $grandtotal && count($activities) > 0) { // Only show CSV if there are some users/actvs

    $shortname = format_string($course->shortname, true, array('context' => $context));
    header('Content-Disposition: attachment; filename=progress.' .
        preg_replace('/[^a-z0-9-]/', '_', core_text::strtolower(strip_tags($shortname))) . '.csv');
    // Unicode byte-order mark for Excel
    if ($excel) {
        header('Content-Type: text/csv; charset=UTF-16LE');
        print chr(0xFF) . chr(0xFE);
        $sep = "\t" . chr(0);
        $line = "\n" . chr(0);
    } else {
        header('Content-Type: text/csv; charset=UTF-8');
        $sep = ",";
        $line = "\n";
    }
} else {

    // Navigation and header
    $strreports = get_string("reports");
    $strcompletion = get_string('pluginname', 'report_attendance');

    $PAGE->set_title($strcompletion);
    $PAGE->set_heading($course->fullname);
    echo $OUTPUT->header();
    $PAGE->requires->js_call_amd('report_attendance/completion_override', 'init', [fullname($USER)]);

    // Handle groups (if enabled)
    groups_print_course_menu($course, $CFG->wwwroot . '/report/attendance/?course=' . $course->id);
}

if (count($activities) == 0) {
    echo $OUTPUT->container(get_string('err_noactivities', 'completion'), 'errorbox errorboxcontent');
    echo $OUTPUT->footer();
    exit;
}

// If no users in this course what-so-ever
if (!$grandtotal) {
    echo $OUTPUT->container(get_string('err_nousers', 'completion'), 'errorbox errorboxcontent');
    echo $OUTPUT->footer();
    exit;
}

// Build link for paging
$link = $CFG->wwwroot . '/report/attendance/?course=' . $course->id;
if (strlen($sort)) {
    $link .= '&amp;sort=' . $sort;
}
$link .= '&amp;start=';

$pagingbar = '';

// Initials bar.
$prefixfirst = 'sifirst';
$prefixlast = 'silast';
$pagingbar .= $OUTPUT->initials_bar($sifirst, 'firstinitial', get_string('firstname'), $prefixfirst, $url);
$pagingbar .= $OUTPUT->initials_bar($silast, 'lastinitial', get_string('lastname'), $prefixlast, $url);

// Do we need a paging bar?
if ($total > COMPLETION_REPORT_PAGE) {

    // Paging bar
    $pagingbar .= '<div class="paging">';
    $pagingbar .= get_string('page') . ': ';

    $sistrings = array();
    if ($sifirst != 'all') {
        $sistrings[] = "sifirst={$sifirst}";
    }
    if ($silast != 'all') {
        $sistrings[] = "silast={$silast}";
    }
    $sistring = !empty($sistrings) ? '&amp;' . implode('&amp;', $sistrings) : '';

    // Display previous link
    if ($start > 0) {
        $pstart = max($start - COMPLETION_REPORT_PAGE, 0);
        $pagingbar .= "(<a class=\"previous\" href=\"{$link}{$pstart}{$sistring}\">" . get_string('previous') . '</a>)&nbsp;';
    }

    // Create page links
    $curstart = 0;
    $curpage = 0;
    while ($curstart < $total) {
        $curpage++;

        if ($curstart == $start) {
            $pagingbar .= '&nbsp;' . $curpage . '&nbsp;';
        } else {
            $pagingbar .= "&nbsp;<a href=\"{$link}{$curstart}{$sistring}\">$curpage</a>&nbsp;";
        }

        $curstart += COMPLETION_REPORT_PAGE;
    }

    // Display next link
    $nstart = $start + COMPLETION_REPORT_PAGE;
    if ($nstart < $total) {
        $pagingbar .= "&nbsp;(<a class=\"next\" href=\"{$link}{$nstart}{$sistring}\">" . get_string('next') . '</a>)';
    }

    $pagingbar .= '</div>';
}

// Okay, let's draw the table of progress info,
// Notes: this code is messy and using bootstrap embedded in the script tag below. todo REFACTOR
// wrapping the table using bootstrap and changed into a dynamic tab.
// Tab header would be the section
// content with no activities is emitted from the tab.

$sections = get_fast_modinfo($COURSE);
$sections = $sections->get_section_info_all();

$csvHeaders = array();
$csvData = array();
$csvSection = array();

// wrapping with tabs
$tabpaging = '';
$contentTab = '';
$actualContent = "";
$tabCounter = 0;
$activityCounter =0;
// script for bootstrap
$tabpaging .= '
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<ul class="nav nav-tabs mb-3" id="nav-tab" role="tablist">';
$contentTab .= "<div class='tab-content' id='pills-tabContent'>";
foreach ($sections as $key => $section) {
    $title = empty($section->name) ? 'Section ' . $key : $section->name;
    if (empty($section->name)) {
        if ($key == 0) {
            $title = 'Introduction';
        } else {
            $title = 'Section ' . $key;
        }
    } else {
        $title = $section->name;
    }
}


foreach ($sections as $key => $section) {
    // Csv arrays
    $csvSectionHeader = array('courseid', 'subjectcode', 'class', 'nim', 'fullname');
    $csvSectionData = array();
    $contentController = "";
    // Start of table
    if (!$csv) {
        $contentController .= '<br class="clearer"/>'; // ugh
        $title = empty($section->name) ? 'Section ' . $key : $section->name;
        if (empty($section->name)) {
            if ($key == 0) {
                $title = 'Introduction';
            } else {
                $title = 'Section ' . $key;
            }
        } else {
            $title = $section->name;
        }
        // print title
//        print '<h3>' . $title . '</h3>';

        if (!$total) {
            echo $OUTPUT->heading(get_string('nothingtodisplay'));
            echo $OUTPUT->footer();
            exit;
        }
    }

    // Activities
    $formattedactivities = array();
    $header = false;
    $activityContentController = "";
    foreach ($activities as $activity) {

        $exclude = array('resource', 'label', 'book', 'url', 'folder', 'page');
        if (in_array($activity->modname, $exclude)) {
            continue;
        }

        if ($activity->section != $section->id) {
            continue;
        }

        if ($header === false) {
            $activityContentController = '<div id="completion-progress-wrapper" class="no-overflow">';
            $activityContentController .= '<table id="completion-progress" class="generaltable flexible boxaligncenter" style="text-align:left"><thead><tr style="vertical-align:top">';

            // User heading
            $activityContentController .= '<th scope="col" class="completion-identifyfield">Course ID</th>';
            $activityContentController .= '<th scope="col" class="completion-identifyfield">Subject Code</th>';
            $activityContentController .= '<th scope="col" class="completion-identifyfield">Class</th>';
            $activityContentController .= '<th scope="col" class="completion-sortchoice">' . get_string('nim', 'report_attendance') . '</th>';
            $activityContentController .= '<th scope="col" class="completion-sortchoice">' . 'Nama' . '</th>';
            $header = true;
        }
        $datepassed = $activity->completionexpected && $activity->completionexpected <= time();
        $datepassedclass = $datepassed ? 'completion-expired' : '';

        if ($activity->completionexpected) {
            $datetext = userdate($activity->completionexpected, get_string('strftimedate', 'langconfig'));
        } else {
            $datetext = '';
        }

        // Some names (labels) come URL-encoded and can be very long, so shorten them
        $displayname = format_string($activity->name, true, array('context' => $activity->context));

        if ($csv) {
            $activityContentController .= $sep . csv_quote($displayname);
        } else {
            $shortenedname = shorten_text($displayname);
            if($activityCounter==0){
                $activityContentController .= '<th scope="col" class="text-center completion-header ' . $datepassedclass . '">' .
                    "Aktifitas" . "<br>" .
                    '<a href="' . $CFG->wwwroot . '/mod/' . $activity->modname .
                    '/view.php?id=' . $activity->id . '" title="' . s($displayname) . '">' .
                    '<div class="rotated-text-container"><span class="rotated-text">' . $shortenedname . '</span></div>' .
                    '<div class="modicon">' .
                    $OUTPUT->image_icon('icon', get_string('modulename', $activity->modname), $activity->modname) .
                    '</div>' .
                    '</a>';
            }else{
                $activityContentController .= '<th scope="col" class="text-center completion-header ' . $datepassedclass . '">' .
                    '<a href="' . $CFG->wwwroot . '/mod/' . $activity->modname .
                    '/view.php?id=' . $activity->id . '" title="' . s($displayname) . '">' .
                    '<div class="rotated-text-container"><span class="rotated-text">' . $shortenedname . '</span></div>' .
                    '<div class="modicon">' .
                    $OUTPUT->image_icon('icon', get_string('modulename', $activity->modname), $activity->modname) .
                    '</div>' .
                    '</a>';
            }

            if ($activity->completionexpected) {
                $activityContentController .= '<div class="completion-expected"><span>' . $datetext . '</span></div>';
            }
            $activityContentController .= '</th>';
        }
        $formattedactivities[$activity->id] = (object)array(
            'datepassedclass' => $datepassedclass,
            'displayname' => $displayname,
        );
        $activityCounter++;
    }
    $activityCounter=0;
    $contentController .= $activityContentController;
    $csvSectionHeader[] = 'attendance';
    if ($csv) {
        $contentController .= $line;
    } else {
        if (empty($formattedactivities)) {
            $contentController = '<h3 style="background-color:gray; color:white">No activities</h3>
             <br class="clearer"/>
            <hr>';
            continue;
        }

        // Attendance Info
        $contentController .= '<th scope="col" class="completion-header text-center">' .
            '<a href="#" title="Attendance">' .
            '<div class="rotated-text-container"><span class="rotated-text">Attendance</span></div>' .
            '</a>' .
            '</th>';

        $contentController .= '</tr></thead><tbody>';
    }

    // Row for each user
    $userContentController = "";
    foreach ($progress as $user) {

        $csvRow = array();
        $userdata = $DB->get_record('user', ['id' => $user->id]);
        $nim = empty($userdata->idnumber) ? '-' : $userdata->idnumber;

        // User name
        if ($csv) {
            $userContentController .= csv_quote($nim) . $sep . csv_quote(fullname($user));
        } else {
            $name = explode('-', $course->shortname, 2);
            $userContentController .= '<tr>';
            $userContentController .= '<td>' . $course->idnumber . '</td>';
            $userContentController .= '<td>' . $name[0] . '</td>';
            $userContentController .= '<td>' . $name[1] . '</td>';
            $userContentController .= '<td>' . $nim . '</td>';
            $userContentController .= '<td><a href="' . $CFG->wwwroot . '/user/view.php?id=' .
                $user->id . '&amp;course=' . $course->id . '">' . fullname($user) . '</a></td>';

            $csvRow[] = $course->idnumber;
            $csvRow[] = $name[0];
            $csvRow[] = $name[1];
            $csvRow[] = $nim;
            $csvRow[] = fullname($user);
        }

        // Progress for each activity
        $completion = 1;
        foreach ($activities as $activity) {
            $progressContentController = "";
            $exclude = array('resource', 'label', 'book', 'url', 'folder', 'page');
            if (in_array($activity->modname, $exclude)) {
                continue;
            }

            if ($activity->section != $section->id) {
                continue;
            }

            // Get progress information and state
            if (array_key_exists($activity->id, $user->progress)) {
                $thisprogress = $user->progress[$activity->id];
                $state = $thisprogress->completionstate;
                $overrideby = $thisprogress->overrideby;
                $date = userdate($thisprogress->timemodified);
            } else {
                $state = COMPLETION_INCOMPLETE;
                $completion = 0;
                $overrideby = 0;
                $date = '';
            }

            // Work out how it corresponds to an icon
            switch ($state) {
                case COMPLETION_INCOMPLETE:
                    $completiontype = 'n' . ($overrideby ? '-override' : '');
                    break;
                case COMPLETION_COMPLETE:
                    $completiontype = 'y' . ($overrideby ? '-override' : '');
                    break;
                case COMPLETION_COMPLETE_PASS:
                    $completiontype = 'pass';
                    break;
                case COMPLETION_COMPLETE_FAIL:
                    $completiontype = 'fail';
                    break;
            }
            $completiontrackingstring = $activity->completion == COMPLETION_TRACKING_AUTOMATIC ? 'auto' : 'manual';
            $completionicon = 'completion-' . $completiontrackingstring . '-' . $completiontype;

            if ($overrideby) {
                $overridebyuser = \core_user::get_user($overrideby, '*', MUST_EXIST);
                $describe = get_string('completion-' . $completiontype, 'completion', fullname($overridebyuser));
            } else {
                $describe = get_string('completion-' . $completiontype, 'completion');
            }
            $a = new StdClass;
            $a->state = $describe;
            $a->date = $date;
            $a->user = fullname($user);
            $a->activity = $formattedactivities[$activity->id]->displayname;
            $fulldescribe = get_string('progress-title', 'completion', $a);

            if ($csv) {
                $progressContentController .= $sep . csv_quote('sample');
            } else {
                $celltext = $OUTPUT->pix_icon('i/' . $completionicon, s($fulldescribe));
                if (has_capability('moodle/course:overridecompletion', $context) &&
                    $state != COMPLETION_COMPLETE_PASS && $state != COMPLETION_COMPLETE_FAIL) {
                    $newstate = ($state == COMPLETION_COMPLETE) ? COMPLETION_INCOMPLETE : COMPLETION_COMPLETE;
                    $changecompl = $user->id . '-' . $activity->id . '-' . $newstate;
                    // $url = new moodle_url($PAGE->url, ['sesskey' => sesskey()]);
                    $url = '#';
                    $celltext = html_writer::div($celltext, 'changecompl', array('data-changecompl' => $changecompl,
                        'data-activityname' => $a->activity,
                        'data-userfullname' => $a->user,
                        'data-completiontracking' => $completiontrackingstring,
                        'aria-role' => 'button'));
                }
                $progressContentController .= '<td class="text-center completion-progresscell ' . $formattedactivities[$activity->id]->datepassedclass . '">' .
                    $celltext . '</td>';
            }
            $userContentController .= $progressContentController;
        }
        if ($csv) {
//            print $line;
        } else {
            // Attendance Info Row
            $status = $completion == 0 ? 'fail' : 'y';
            $url = new \moodle_url('/theme/image.php/boost/core/1556006832/i/completion-auto-' . $status);
            $userContentController .= '<td class="text-center completion-progresscell">' .
                '<img class="icon " src="' . $url . '">'
                . '</td>';
            $userContentController .= '</tr>';
            $csvRow[] = $completion;
        }
        $csvSectionData[] = $csvRow;
    }
    $contentController .= $userContentController;
    if ($csv) {
        exit;
    }
    $contentController .= '</tbody></table>';
    $contentController .= '</div>';
    //print $pagingbar;
    $path = 'output/' . time() . '-attendance-' . $course->shortname . '-' . $section->id . '.csv';
    $contentController .= '<br class="clearer"/>';
    $contentController .= '<a href="' . $path . '"style="float:right">' . '<button type="button" class="btn btn-success">' . 'Download Report section : ' . $title . '</button>' . '</a>';
    $contentController .= '<br class="clearer"/>';
    $csvHeaders[] = $csvSectionHeader;
    $csvData[] = $csvSectionData;
    if (!empty($csvSectionData)) {
        $csvSection[] = $section->id;
    }
    $contentController .= '<hr>';
    $tabpaging .= '<li class="nav-items">';
    if ($tabCounter == 0) {
        $tabpaging .= "&nbsp;<a class='nav-link active show' id='pills-{$key}-tab' data-toggle='pill' href='#pills-{$key}' role='tab' aria-controls='pills-{$key}' aria-selected='true'>" . $title . "</a>";
        $contentTab .= "<div class='tab-pane fade show active' id='pills-{$key}' role='tabpanel' aria-labelledby='pills-{$key}-tab'>";
    } else {
        $tabpaging .= "&nbsp;<a class='nav-link' id='pills-{$key}-tab' data-toggle='pill' href='#pills-{$key}' role='tab' aria-controls='pills-{$key}' aria-selected='true'>" . $title . "</a>";
        $contentTab .= "<div class='tab-pane fade' id='pills-{$key}' role='tabpanel' aria-labelledby='pills-{$key}-tab'>";
    }
    $tabpaging .= "</li>";
    $tabCounter++;
    $contentTab .= $contentController;
    $contentTab .= "</div>";
}
echo $tabpaging;
print "</ul>";
print $contentTab;
print "</div>";

// Output csv
$path = 'output/' . time() . '-attendance-' . $course->shortname . '.csv';
$fp = fopen($path, 'w');


// Output data
foreach ($csvHeaders as $key => $row) {
    $pathDetail = 'output/' . time() . '-attendance-' . $course->shortname . '-' . $csvSection[$key] . '.csv';
    $fpDetail = fopen($pathDetail, 'w');
    fputcsv($fp, $row);
    fputcsv($fpDetail, $row);

    foreach ($csvData[$key] as $data) {
        fputcsv($fp, $data);
        fputcsv($fpDetail, $data);
    }
    fclose($fpDetail);
}

fclose($fp);
print '<a href="' . $path . '">' . '<button type="button" class="btn btn-warning">' . 'Download all section report' . '</button></a>';

echo $OUTPUT->footer();
