<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Activity attendance reports
 *
 * @package    report
 * @subpackage attendance
 * @copyright  2008 Sam Marshall
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once($CFG->libdir . '/completionlib.php');

define('COMPLETION_REPORT_PAGE', 500);

// Get course
$id = required_param('course', PARAM_INT);
$cmsids = optional_param_array('cmid', [], PARAM_INT);
$course = $DB->get_record('course', array('id' => $id));
$context = context_course::instance($course->id);
$modinfo = get_fast_modinfo($id);
$manager = new \core_completion\manager($course->id);

if (!core_completion\manager::can_edit_bulk_completion($course)) {
    require_capability('moodle/course:manageactivities', context_course::instance($course->id));
}

$forum = [];
$assignment = [];
//foreach ($cmGetter as $mod) {
//    if ($mod->modfullname == 'Forum') {
//        $forum[$mod->id] = $modinfo->get_cm($mod->id);
//    } elseif ($mod->modfullname == 'Assignment') {
//        $assignment[$mod->id] = $modinfo->get_cm($mod->id);
//    } elseif ($mod->modfullname == 'Quiz') {
//        array_push($quiz, $mod->id);
//    } else {
//        array_push($hvp, $cm->id);
//    }
//    array_push($cms, $cm->id);
//}

if (!$course) {
    print_error('invalidcourseid');
}
$url = new moodle_url('/report/attendance/setup.php', array('course' => $id));

$PAGE->set_url($url);

$PAGE->set_pagelayout('admin');

// Check basic permission
require_capability('report/attendance:view', $context);

// Get data on activities and progress of all users, and give error if we've
// nothing to display (no users or no activities)
$reportsurl = $CFG->wwwroot . '/course/report.php?id=' . $course->id;
$completion = new completion_info($course);

// Navigation and header
$strreports = get_string("reports");
$strcompletion = get_string('pluginname', 'report_attendance');
$cmGetter = $modinfo->get_cms();

foreach ($cmGetter as $mod) {

    if ($mod->modfullname == 'Forum') {
        $forum[$mod->id] = $modinfo->get_cm($mod->id);
    } elseif ($mod->modfullname == 'Assignment') {
        $assignment[$mod->id] = $modinfo->get_cm($mod->id);
    } elseif ($mod->modfullname == 'Quiz') {
        array_push($quiz, $mod->id);
    } else {
        array_push($hvp, $cm->id);
    }
    array_push($cms, $cm->id);
}

$returnurl = new moodle_url('/index.php', ['id' => $course->id]);
$manager = new \core_completion\manager($course->id);
//if (empty($cms)) {
////    redirect($returnurl);
////}
$form_forum = new core_completion_bulkedit_form(null, ['cms' => $forum]);
$form_assignment = new core_completion_bulkedit_form(null,['cms'=>$assignment]);
if ($form_forum->is_cancelled()) {
    redirect($returnurl);
} else if ($data_forum = $form_forum->get_data() and $data_assignment = $form_assignment->get_data()) {
    $manager->apply_completion($data_forum, $form_forum->has_custom_completion_rules());
    $manager->apply_completion($data_assignment,$form_assignment->has_custom_completion_rules());
    redirect($returnurl);
}
$renderer = $PAGE->get_renderer('core_course', 'bulk_activity_completion');

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('bulkactivitycompletion', 'completion'));
echo json_encode($assignment);
echo $renderer->edit_bulk_completion($form_forum, $manager->get_activities(array_keys($forum)));
echo $renderer->edit_bulk_completion($form_assignment,$manager->get_activities(array_keys($assignment)));
echo $OUTPUT->footer();