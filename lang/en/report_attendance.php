<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Lang strings
 *
 * @package    report
 * @subpackage attendance
 * @copyright  2008 Sam Marshall
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Attendance report';
$string['page-report-attendance-x'] = 'Any attendance report';
$string['page-report-attendance-index'] = 'Attendance report';
$string['page-report-attendance-user'] = 'User attendance report';
$string['privacy:metadata'] = 'The Attendance report only shows data stored in other locations.';
$string['attendance:view'] = 'View attendance reports';

$string['nim'] = 'NIM';
$string['fullname'] = 'Full name';
