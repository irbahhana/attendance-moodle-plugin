# Attendance for Moodle
Moodle plugin for attandence recoding in PHP. This attendance plugin is based on [this attendance](https://moodle.org/plugins/mod_attendance?__cf_chl_captcha_tk__=02e39c624e6488d37954493fb8f755722b7ee7b6-1626527775-0-Af52vJRZpKWvEYNSmeswPVi1N8RVA6s-gzkicWx5SslKD9WjVzF6pWrHhH-XrdO4sfRTiP-rcSmBd-iPZZ_Rgp0F86MuGtM5bK9AAQfi9UrCOQnx9XVF-qy5SNbI3ZrOkIbal7xI98AQzaePsrz5gZnUQ-7X5sfIh_QB-eoWN8t-obo2nKFhVM11XGSxt24-5FMilpciOE9vFe-n_EZ43y8_EZHFNeiZER3gqniMlQt84LPBbANylyxufBT3dhIKo7ffjCHG6I8maGstESIORzszESgdlKPFplBfalgg3fiwMzzjrNVVBAPQzgPeFVUxVfclSX3PzdyNwdUGf0rVdpQX7v-LdsOR_FDNYYLkYetq_35nXljS2QdkiaQtPxYOHVD0Y77uoPqCWZYcpJOfObkPaDl_z9Nz0k9NG_UOwsHOGaSXM8LAkgFpbzRpvZdvTmLImPLqt1nFaa2VGpHNmv4plMynlY9OAqRj9T_Mvm8FOW63YfyoCXp0DxTnTdPr2dChXtQB60btHud6EidP7vZEg6yNt53l5NdFj8RRyYNuhXfkX5xKgqX9lDR9kiOmRhTB5pzzginAaYbwMjy_2wGM48vhIz5HJP74yX76R-wo3vlTQMu8eU48tOjD41nEv5ETSe8LPdtaJuXQs7wrOy20IlIk8cmAlm3PmSRvaXE02_DtlxIMqDNhAQiNaVBKZQwkYpp9tIAULnFYSwq9vxI) plugin in moodle.

##CHANGELOG:
- ver 0.2  
TWEAK - added string "aktifitas" on the upper first of activity table to indicate the activty
- ver 0.1  
TWEAK - added bootstrap script for ease of tab using  
TWEAK - emitting section with no activity in the tab

## Installing
Refer to [Moodle documetation](https://docs.moodle.org/311/en/Installing_plugins#Installing_a_plugin) for direction
